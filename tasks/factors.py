def factors(number):
    list_of_numbers = []  # Empty list initialised
    for factor in range(2, number):  # Gets all factors of N bar itself and 1
        if number % factor == 0:  # If the number has no remainder
            list_of_numbers.append(factor)  # Add factors to list
    return list_of_numbers


print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “[]” (an empty list) to the console
